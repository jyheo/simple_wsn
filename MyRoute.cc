//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "MyRoute.h"

#include "MyRoutePkt_m.h"
#include "ArpInterface.h"
#include "NetwControlInfo.h"
#include "MacToNetwControlInfo.h"

using std::make_pair;

Define_Module(MyRoute);

void MyRoute::initialize(int stage)
{
    // TODO - Generated method body
    BaseNetwLayer::initialize(stage);

    if (stage == 1) {
        seqn = 0;
    }
}

void MyRoute::handleUpperMsg(cMessage *msg)
{
    // TODO - Generated method body
    LAddress::L3Type finalDestAddr;
    LAddress::L2Type nextHopMacAddr;

    cObject*         cInfo = msg->removeControlInfo();
    if ( cInfo == NULL ) {
        EV << "MyRoute warning: Application layer did not specify a destination L3 address\n"
           << "\tDrop It\n";
        delete msg;
        return;
    } else {
        EV <<"MyRoute: CInfo removed, netw addr="<< NetwControlInfo::getAddressFromControlInfo( cInfo ) <<endl;
        finalDestAddr = NetwControlInfo::getAddressFromControlInfo( cInfo );
        delete cInfo;
    }

    MyRoutePkt*    pkt = new MyRoutePkt(msg->getName(), 1); // message type = 1
    pkt->setByteLength(headerLength);

    pkt->setFinalDestAddr(finalDestAddr);
    pkt->setInitialSrcAddr(myNetwAddr);
    pkt->setSrcAddr(myNetwAddr);
    pkt->setDestAddr(LAddress::L3BROADCAST);
    pkt->setSeqNum(seqn);
    seqn++;

    nextHopMacAddr = LAddress::L2BROADCAST; //arp->getMacAddr(LAddress::L3BROADCAST);

    setDownControlInfo(pkt, nextHopMacAddr);
    pkt->encapsulate(static_cast<cPacket*>(msg));
    sendDown(pkt);
}

void MyRoute::handleLowerMsg(cMessage *msg)
{
    // TODO - Generated method body
    MyRoutePkt*           netwMsg        = check_and_cast<MyRoutePkt*>(msg);
    const LAddress::L3Type& finalDestAddr  = netwMsg->getFinalDestAddr();
    const LAddress::L3Type& initialSrcAddr = netwMsg->getInitialSrcAddr();
    const LAddress::L3Type& srcAddr        = netwMsg->getSrcAddr();
    const unsigned long seq_num = netwMsg->getSeqNum();
    //double rssi = static_cast<MacToNetwControlInfo*>(netwMsg->getControlInfo())->getRSSI();
    //double ber = static_cast<MacToNetwControlInfo*>(netwMsg->getControlInfo())->getBitErrorRate();

    if (initialSrcAddr == myNetwAddr) {
        delete netwMsg;
        return;
    }

    last_seqn_of_node_t::iterator pos = last_seqn_of_node.find(initialSrcAddr);
    if (pos == last_seqn_of_node.end()) {
        // no node in the list
        last_seqn_of_node.insert(make_pair(initialSrcAddr, seq_num));
    } else {
        unsigned long last_seqn = pos->second;
        if (last_seqn >= seq_num) {
            // drop this message. it is already received or outdated.
            delete netwMsg;
            return;
        } else {
            pos->second = seq_num;
        }
    }

    if (finalDestAddr == myNetwAddr) {
        // send packet to the upper layer
        cMessage *m = netwMsg->decapsulate();
        setUpControlInfo(m, netwMsg->getSrcAddr());
        delete netwMsg;
        sendUp(m);
    } else {
        // re-broadcast
        netwMsg->setSrcAddr(myNetwAddr);
        netwMsg->setDestAddr(LAddress::L3BROADCAST);
        cObject *cInfo = netwMsg->removeControlInfo();
        if (cInfo)
            delete cInfo;
        setDownControlInfo(netwMsg, LAddress::L2BROADCAST);
        sendDown(netwMsg);
    }
}
