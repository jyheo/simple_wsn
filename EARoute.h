/***************************************************************************
 * file:        EARoute.h
 *
 * author:      Damien Piguet, Jerome Rousselot
 *
 * copyright:   (C) 2007-2009 CSEM SA, Neuchatel, Switzerland.
 *
 * description: Implementation of the routing protocol of WiseStack.
 *
 *              This program is free software; you can redistribute it
 *              and/or modify it under the terms of the GNU General Public
 *              License as published by the Free Software Foundation; either
 *              version 2 of the License, or (at your option) any later
 *              version.
 *              For further information see file COPYING
 *              in the top level directory
 *
 * Funding: This work was partially financed by the European Commission under the
 * Framework 6 IST Project "Wirelessly Accessible Sensor Populations"
 * (WASP) under contract IST-034963.
 ***************************************************************************
 * ported to Mixim 2.0.1 by Theodoros Kapourniotis
 * last modification: 06/02/11
 **************************************************************************/

#ifndef earoute_h
#define earoute_h

#include <map>
#include <omnetpp.h>

#include "MiXiMDefs.h"
#include "BaseNetwLayer.h"
#include "SimpleAddress.h"

class SimTracer;
class EARoutePkt;

/**
 * @brief EARoute is an energy aware routing.
 * EARoute maintain multi-path to the sink in the routing table.
 *
 * Shah, Rahul C., and Jan M. Rabaey.
 *  "Energy aware routing for low energy ad hoc sensor networks."
 *   Wireless Communications and Networking Conference, 2002.
 *
 *
 * @ingroup netwLayer
 * @author Junyoung Heo
 **/
class MIXIM_API EARoute : public BaseNetwLayer
{
private:
	/** @brief Copy constructor is not allowed.
	 */
	EARoute(const EARoute&);
	/** @brief Assignment operator is not allowed.
	 */
	EARoute& operator=(const EARoute&);

public:
	EARoute()
		: BaseNetwLayer()
        , weight_alpha(1)
        , weight_beta(1)
		, routeTable()
		, headerLength(0)
		, macaddress()
		, sinkAddress()
		, routeFloodsInterval(0)
		, floodSeqNumber(0)
		, routeFloodTimer(NULL)
        , nbForwardedPacket(0)
		, stats(false), debug(false)
	{}
    /** @brief Initialization of the module and some variables*/
    virtual void initialize(int);
    virtual void finish();

    virtual ~EARoute();

protected:
	enum messagesTypes {
	    UNKNOWN=0,
	    DATA,
	    ROUTE_FLOOD,
	    SEND_ROUTE_FLOOD_TIMER
	};

	double weight_alpha, weight_beta;

	typedef struct tNextHopInfo {
	    double Prob;
	    double Cost_link; // cost of link between me and next hop = Cost of next hop + Metric
	    double rssi;
	} tNextHopInfo;

	typedef struct tRouteTableEntry {
	    std::map<LAddress::L3Type, tNextHopInfo> nextHopInfo; // next hop addr -> hop info
	    double Cost;
	} tRouteTableEntry;

	typedef std::map<LAddress::L3Type, tRouteTableEntry>        tRouteTable;
	typedef std::map<LAddress::L3Type, tRouteTableEntry>::iterator        tRouteTable_ptr;

	tRouteTable routeTable;

    /**
     * @brief Length of the NetwPkt header
     * Read from omnetpp.ini
     **/
    int headerLength;

    /** @brief cached variable of my network address */
//    int myNetwAddr;
    LAddress::L2Type macaddress;

    LAddress::L3Type sinkAddress;

    /** @brief Interval [seconds] between two route floods. A route flood is a simple flood from
     *         which other nodes can extract routing (next hop) information.
     */
    double routeFloodsInterval;

    /** @brief Flood sequence number */
    unsigned long floodSeqNumber;

    cMessage* routeFloodTimer;

    bool stats, debug;

    void updateCost();
    double getCost(const LAddress::L3Type& origin);

    int nbForwardedPacket;

    /**
     * @name Handle Messages
     * @brief Functions to redefine by the programmer
     *
     * These are the functions provided to add own functionality to your
     * modules. These functions are called whenever a self message or a
     * data message from the upper or lower layer arrives respectively.
     *
     **/
    /*@{*/

    /** @brief Handle messages from upper layer */
    virtual void handleUpperMsg(cMessage* msg);

    /** @brief Handle messages from lower layer */
    virtual void handleLowerMsg(cMessage* msg);

    /** @brief Handle self messages */
    virtual void handleSelfMsg(cMessage* msg);

    /** @brief Handle control messages from lower layer */
    virtual void handleLowerControl(cMessage* msg);

    /** @brief Update routing table.
     *
     * The tuple provided in argument gives the next hop address to the origin.
     */
    virtual bool updateRouteTable(const tRouteTable::key_type& origin, const LAddress::L3Type& lastHop, double rssi, double Cost_link);

    /** @brief find a route to destination address. */
    virtual LAddress::L3Type getRoute(const LAddress::L3Type& destAddr, bool iAmOrigin = false);

    /** @brief Decapsulate a message */
    cMessage* decapsMsg(EARoutePkt *msg);
};

#endif
