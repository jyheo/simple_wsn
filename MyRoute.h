//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __SIMPLE_WSN_MYROUTE_H_
#define __SIMPLE_WSN_MYROUTE_H_

#include <omnetpp.h>
#include <list>
#include <map>

#include "BaseNetwLayer.h"

/**
 * TODO - Generated class
 */
class MyRoute : public BaseNetwLayer
{
    unsigned long seqn;
    typedef std::map<LAddress::L3Type, unsigned long> last_seqn_of_node_t;
    last_seqn_of_node_t last_seqn_of_node;

  protected:
    virtual void initialize(int);

    /** @brief Handle messages from upper layer */
    virtual void handleUpperMsg(cMessage* msg);

    /** @brief Handle messages from lower layer */
    virtual void handleLowerMsg(cMessage* msg);
};

#endif
