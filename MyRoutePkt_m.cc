//
// Generated file, do not edit! Created by opp_msgc 4.3 from MyRoutePkt.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "MyRoutePkt_m.h"

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




Register_Class(MyRoutePkt);

MyRoutePkt::MyRoutePkt(const char *name, int kind) : NetwPkt(name,kind)
{
}

MyRoutePkt::MyRoutePkt(const MyRoutePkt& other) : NetwPkt(other)
{
    copy(other);
}

MyRoutePkt::~MyRoutePkt()
{
}

MyRoutePkt& MyRoutePkt::operator=(const MyRoutePkt& other)
{
    if (this==&other) return *this;
    NetwPkt::operator=(other);
    copy(other);
    return *this;
}

void MyRoutePkt::copy(const MyRoutePkt& other)
{
    this->finalDestAddr_var = other.finalDestAddr_var;
    this->initialSrcAddr_var = other.initialSrcAddr_var;
}

void MyRoutePkt::parsimPack(cCommBuffer *b)
{
    NetwPkt::parsimPack(b);
    doPacking(b,this->finalDestAddr_var);
    doPacking(b,this->initialSrcAddr_var);
}

void MyRoutePkt::parsimUnpack(cCommBuffer *b)
{
    NetwPkt::parsimUnpack(b);
    doUnpacking(b,this->finalDestAddr_var);
    doUnpacking(b,this->initialSrcAddr_var);
}

LAddress::L3Type& MyRoutePkt::getFinalDestAddr()
{
    return finalDestAddr_var;
}

void MyRoutePkt::setFinalDestAddr(const LAddress::L3Type& finalDestAddr)
{
    this->finalDestAddr_var = finalDestAddr;
}

LAddress::L3Type& MyRoutePkt::getInitialSrcAddr()
{
    return initialSrcAddr_var;
}

void MyRoutePkt::setInitialSrcAddr(const LAddress::L3Type& initialSrcAddr)
{
    this->initialSrcAddr_var = initialSrcAddr;
}

class MyRoutePktDescriptor : public cClassDescriptor
{
  public:
    MyRoutePktDescriptor();
    virtual ~MyRoutePktDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(MyRoutePktDescriptor);

MyRoutePktDescriptor::MyRoutePktDescriptor() : cClassDescriptor("MyRoutePkt", "NetwPkt")
{
}

MyRoutePktDescriptor::~MyRoutePktDescriptor()
{
}

bool MyRoutePktDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<MyRoutePkt *>(obj)!=NULL;
}

const char *MyRoutePktDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int MyRoutePktDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int MyRoutePktDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISCOMPOUND,
        FD_ISCOMPOUND,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *MyRoutePktDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "finalDestAddr",
        "initialSrcAddr",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int MyRoutePktDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='f' && strcmp(fieldName, "finalDestAddr")==0) return base+0;
    if (fieldName[0]=='i' && strcmp(fieldName, "initialSrcAddr")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *MyRoutePktDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "LAddress::L3Type",
        "LAddress::L3Type",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *MyRoutePktDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int MyRoutePktDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    MyRoutePkt *pp = (MyRoutePkt *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string MyRoutePktDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    MyRoutePkt *pp = (MyRoutePkt *)object; (void)pp;
    switch (field) {
        case 0: {std::stringstream out; out << pp->getFinalDestAddr(); return out.str();}
        case 1: {std::stringstream out; out << pp->getInitialSrcAddr(); return out.str();}
        default: return "";
    }
}

bool MyRoutePktDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    MyRoutePkt *pp = (MyRoutePkt *)object; (void)pp;
    switch (field) {
        default: return false;
    }
}

const char *MyRoutePktDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        "LAddress::L3Type",
        "LAddress::L3Type",
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *MyRoutePktDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    MyRoutePkt *pp = (MyRoutePkt *)object; (void)pp;
    switch (field) {
        case 0: return (void *)(&pp->getFinalDestAddr()); break;
        case 1: return (void *)(&pp->getInitialSrcAddr()); break;
        default: return NULL;
    }
}


